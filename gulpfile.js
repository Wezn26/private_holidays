
var syntax        = 'scss';
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    terser = require('gulp-terser'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer');

//INSTALL
//yarn add gulp gulp-sass browser-sync gulp-concat gulp-terser gulp-cssnano gulp-rename del gulp-imagemin imagemin-pngquant gulp-cache gulp-autoprefixer

         // GULP TASKS BEGINER
//SASS
gulp.task('sass', function() {
  return gulp.src('app/sass/**/*.scss')
  .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
  .pipe(autoprefixer(['last 15 version', '>1%', 'ie 8', 'ie 7'], {cascade: true}))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({stream: true}));
});

//SCRIPTS
gulp.task('scripts', function() {
  return gulp.src([
    'app/js/script.js'
   ])
  .pipe(concat('libs.min.js'))
  .pipe(terser())
  .pipe(gulp.dest('app/js'))
  .pipe(browserSync.reload({ stream: true }));
});

gulp.task('code', function() {
	return gulp.src('app/*.html')
	.pipe(browserSync.reload({ stream: true }))
});

//CSS-LIBS
gulp.task('css-libs', gulp.parallel('sass'), function() {
  return gulp.src('app/css/libs.css')
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('app/css'));
});
//CSS-MIN
gulp.task('css-min', function() {
  return gulp.src('app/css/main.css')
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({ stream: true }));
});

//BROWSER-SYNC
gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  notify: false
  });
});

//CLEAN
gulp.task('clean', function() {
  return del.sync('dist');
});

//CLEAR
gulp.task('clear', function() {
  return cache.clearAll();
});

//IMAGES
gulp.task('img', function() {
   return gulp.src('app/img/**/*')
        .pipe(cache(imagemin({
          interlaced: true,
          progressive: true,
          svgoPlugins: [{removeViewBox: false}],
          use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'));
});


//WATCH
gulp.task('watch', function() {
		gulp.watch('app/sass/**/*.scss', gulp.parallel('sass'));
		gulp.watch('js/**/*.js', gulp.parallel('scripts'));
		gulp.watch('app/*.html', gulp.parallel('code'))
	});
gulp.task('default', gulp.parallel('sass', 'scripts', 'browser-sync', 'watch'));


//PRE BUILD
gulp.task('prebuild', async function() {
  var buildCss = gulp.src([
    'app/css/main.min.css'
   ])
  .pipe(gulp.dest('dist/css'));

  var buildFonts = gulp.src('app/fonts/**/*')
     .pipe(gulp.dest('dist/fonts'));

  var buildJs = gulp.src('app/js/**/*')
      .pipe(gulp.dest('dist/js'));

  var buildHtml = gulp.src('app/*.html')
      .pipe(gulp.dest('dist'));
});

//BUILD
gulp.task('build', gulp.parallel('prebuild', 'img', 'sass', 'scripts'));




      //GULP TASKS END

      // gulp.task('prebuild', async function() {
      //
      // 	var buildCss = gulp.src([ // Переносим библиотеки в продакшен
      // 		'app/css/main.min.css'
      // 		])
      // 	.pipe(gulp.dest('dist/css'))
      //
      // 	var buildFonts = gulp.src('app/fonts/**/*') // Переносим шрифты в продакшен
      // 	.pipe(gulp.dest('dist/fonts'))
      //
      // 	var buildJs = gulp.src('app/js/**/*') // Переносим скрипты в продакшен
      // 	.pipe(gulp.dest('dist/js'))
      //
      // 	var buildHtml = gulp.src('app/*.html') // Переносим HTML в продакшен
      // 	.pipe(gulp.dest('dist'));
      //
      // });
